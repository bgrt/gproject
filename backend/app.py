from flask import Flask, request
from flask_cors import CORS, cross_origin
import json

app = Flask(__name__)
CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/hellofromflask")
def hello_from_flask():
    return "Hello World from Flask!"

@app.route("/calculate_difference", methods = ['POST'])
def calc():
    content = request.get_json()
    result = content['x'] - content['y']
    return json.dumps(result)

if __name__ == "__main__":
    app.run()
