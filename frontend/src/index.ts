async function sayHelloToBackend() {
    let response = await fetch("http://localhost:5000/hellofromflask");
    return response.text();
}

async function main() {
    let res = await sayHelloToBackend()
    let node = document.getElementById("backend-call").innerHTML = res;

    let result = await calculate(10, 5)
    console.log("10 - 5 = " + result);
}

async function calculate(x: number, y: number) {
    let response = await fetch("http://localhost:5000/calculate_difference", {
        method: "POST", 
        body: JSON.stringify({
            'x': x,
            'y': y
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    });

    return response.text();
}

main();
